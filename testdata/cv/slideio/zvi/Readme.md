# Test images
Images in this folder are used for testing of the library. 
Sources of images:
[OpenSlide project](http://openslide.cs.cmu.edu/download/openslide-testdata/Zeiss/)
 - Zeiss-1-Merged.zvi
 - Zeiss-1-Stacked.zvi