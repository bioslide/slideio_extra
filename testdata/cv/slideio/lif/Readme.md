# Test images
Images in this folder are used for testing of the library. 
Sources of images:
[openmicroscopy project](https://downloads.openmicroscopy.org/images/Leica-LIF/michael/)
- 150519_FRAP_test_ROIs_chromagreen.lif
- 20191025 Test FRET 585. 423, 426.lif
- PR2729_frameOrderCombinedScanTypes.lif