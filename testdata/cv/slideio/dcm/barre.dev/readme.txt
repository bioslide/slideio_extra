Sample DICOM files (c) by Sébastien Barré

The original files were obtained from: 
https://barre.dev/medical/samples/

These files are free of known copyright restrictions and in the public domain.
See the Creative Commons Public Domain Mark page for usage details,
<http://creativecommons.org/publicdomain/mark/1.0/>.